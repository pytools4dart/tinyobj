# tinyobj : Python binding to C++ tinyobjloader.

Files copied from https://github.com/syoyo/tinyobjloader.

## Requirements
Package `pybind11`, required for install, can be installed from pip:

```
pip install pybind11
```

### python 2.7 on Windows
For python 2.7 on windows, the use of pybind11 also requires MSVC v140 (Visual Studio 2015) and Windows SDK
which can be installed with visual studio installer.
Within the VS 2019 installer, select the corresponding components in C++ Development Desktop

Finally it has to be set with the following command lines 
(see [pybind/python_example](https://github.com/pybind/python_example))):

```commandLine
"%VS140COMNTOOLS%\..\..\VC\vcvarsall.bat" x64
set DISTUTILS_USE_SDK=1
set MSSdk=1
```

## Install

Package `tinyobj` can then be installed with ommand line:

```
pip install git+https://gitlab.irstea.fr/florian.deboissieu/tinyobj.git 
```



