from distutils.core import setup, Extension
from pybind11 import get_include

# `../tiny_obj_loader.cc` contains implementation of tiny_obj_loader.
m = Extension('tinyobj',
              sources = ['src/bindings.cc', 'src/tiny_obj_loader.cc'],
              extra_compile_args=['-std=c++11'],
              include_dirs = ['src/', get_include()]
              )


setup (name = 'tinyobj',
       version = '0.1',
       description = 'Python module for tinyobjloader',
       ext_modules = [m])


